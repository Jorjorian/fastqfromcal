Running you need to run all python combineAllfovCals.py , 

command line arguments are:
-c path_to_Online_Intdata 
-t output_directory 
-j argument json ( see template)
-H (optional, thread counts) 

In the template json you specify.

slides ["slide_name"]

lanes ["L01",] list of lanes to include

esr: boolean, true or false

cycles: "r1" for se "r1_r2" for PE, this string specifies how many cycles to include in each srand fastq

gt_cycles: these are the ground truth cycles, which allows for compensation for error correction and for us to know how long the strands actually are. they are ussually 1 cycle longer than the designation, so for SE100 gt_cycles would be "101, for pe150 "151_151". 

offsets: how many cycles offset from the begining of each strand should the window of cycles in each fastq be, ussually "0" or "0_0" but if you want to skip the first 10 cycles of strand 1 it would be "10_0"


barcode: how long is the barcode, 0 if there is not barcode. 
