import os
import shutil
import subprocess
import glob
import json
import time
import socket
server = socket.gethostname().lower().split('.')[0]
CalDirList = ['//prod/hustor-01/zebra/ZebraV2.1/Online/IntData',
              '//prod/hustor-02/zebra/ZebraV2.1/Online/IntData',
              '//prod/pv-04/ZebraV2.1/Online/IntData']
cal_vers = {'Cal': True, 'calFile': False}


def main():
    search_dir = os.path.join('//', 'prod', 'hustor-01', 'zebra', 'Zebra_Temp', 'FastqRegenRequests', server)
    while True:
        requests = glob.glob(os.path.join(search_dir, '*.json'))
        if len(requests) > 0:
            for request in requests:
                with open(request, 'r') as rf:
                    request_dict = json.load(rf, encoding='utf-8')
                
                slide = request_dict['slide']
                lanes = request_dict['lanes']
                out_path = '//prod/pv-10/home/ajorjorian/fastq_temps'
                for int_path in CalDirList:
                    print(int_path)
                    for cal_version in ['Cal', 'calFile']:
                        met_path = os.path.join(int_path, slide[0], lanes[0],
                                                cal_version)
                        if os.path.isdir(met_path):
                            if len(os.listdir(met_path)) > 0:
                                lite = cal_vers[cal_version]
                                request_dict['Lite'] = lite
                                # with open(request, 'w') as rf:
                                #     json.dump(request_dict, rf, encoding='utf-8')
                                break
                if not os.path.isdir(out_path):
                    os.makedirs(out_path)
                a = subprocess.Popen(['python', 'combineAllfovCals.py', '-c', int_path, '-t', out_path, '-j', request],
                                     cwd='Cal2Fastq')
                a.wait()
                os.remove(request)

        else:
            time.sleep(300)


if __name__=='__main__':
    main()

