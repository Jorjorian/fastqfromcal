from fovReaderLite import FovReaderLite
import utilities as util
import numpy as np
import os, sys
import gzip

'''
    Goals:
        Add PE cal file reading,
        Add PE cycle cal file reading

        Add PE fq file writing
'''

class CalReaderLite(FovReaderLite):
    """
        API for reading cal files generated by the Basecaller Lite

        Attributes:

        Public Methods:
            readCal(cycles): Read the raw byte data of the cal file
    """

    def __init__(self, filePaths=None, bases="ACGTN"):
        if isinstance(filePaths, (list, tuple, np.ndarray)) and len(filePaths) > 1:
            super(CalReaderLite, self).__init__(filePaths[0])
            self.calCount = len(filePaths)
        else:
            super(CalReaderLite, self).__init__(filePaths)
            self.calCount = 1
        self.calPaths = filePaths
        self.writeMode = 'w'

        self._encodeB2S6 = lambda base, score: (base << 6 | score) & 0xFF
        self.bases = list(bases)
        self.BaseIdx = dict([(base, idx) for idx, base in enumerate(self.bases)])

        #ESR Filter Parameters
        self.qThreshold = [20, 15]
        self.esrCycleLimit = 2
        self.esrCycleNbr = 20
        return

    ###########################################################################
    #Encoding and Decoding Cal Bytes                                          #
    ###########################################################################
    def _decodeB2S6(self, cal):
        parts = util._bundle(score = 0x3F & cal)
        base = cal >> 6
        base[parts.score == 0] = 4
        parts.base = base
        return parts

    def _decode(self, cal):
        parts = self._decodeB2S6(cal)
        #Transpose parts to (dnb x cycle shape)
        return parts.score.T, parts.base.T

    def _encode(self, base, score):
        cal = self._encodeB2S6(base, score)
        return cal

    def _chunkError(self, data, chunk_id):
        data[:] = self._encode(self.BaseIdx["N"], 0)
        return

    
    ###########################################################################
    #Read and Interpret Cal Data from Cal File                                #
    ###########################################################################
    def _decodeCal(self, cycles=None, ascii_fmt=False):
        cal = self.readCal(cycles)
        score, base = self._decode(cal)
        if ascii_fmt:
            base, score = self.int_to_ascii(base, score)
        return base, score

    def readCal(self, cycles=None, cal_idx=0):
        '''
            Read raw byte data from cal file (bytes are encoded with the base and score value)

            Parameters:

                cycles: Argument types:
                        None: All cycles
                  [int, int]: Start and end of cycle range (1-indexed)

            Return:
        '''
        data = self._readData(cycles, np.uint8)[:, 0, :]
        return data

    def int_to_ascii(self, base_i, score_i):
        base = np.array(self.bases)[base_i]
        score = (score_i + ord('!')).view('S1')
        return base, score

    def _generateEsrMask(self, score, strand, q_threshold=20, cycle_limit=2, cycle_nbr=20):
        #TODO:Priority add PE Esr filter
        dropped_cycles = np.all(score == 0, axis=0)
        bad_cycle_count = (score[:, :cycle_nbr] < q_threshold).sum(axis=1)
        esr_mask = (bad_cycle_count <= cycle_limit)
        return esr_mask

    def _createDnbSet(self, dnb_subset, esr_mask):
        if not dnb_subset:
            dnbs = np.arange(base.shape[0])[esr_mask]
        elif type(dnb_subset) is list:
            dnbs = np.array(dnb_subset) - 1
            dnbs = [dnb for dnb in dnbs if esr_mask[dnb]]
        else:
            #DNB subset is a boolean mask
            dnbs = np.where(dnb_subset & esr_mask)
        return set(dnbs)

    def memFq(self, cycles=None, strand=None, dnb_subset=False, esr_filter=False):
        base, score = self._decodeCal(cycles, False)
        if esr_filter:
            if str(strand) in ["None", "1"]:
                esr_mask = self._generateEsrMask(score, strand, self.qThreshold[0],
                                                self.esrCycleLimit, self.esrCycleNbr)
            else:
                esr_mask = self._generateEsrMask(score, strand, self.qThreshold[1],
                                                self.esrCycleLimit, self.esrCycleNbr)
        else:
            esr_mask = np.ones(score.shape[0], dtype=bool)
        base, score = self.int_to_ascii(base, score)
        base  = np.ascontiguousarray(base).view("S{}".format(base.shape[1]))
        score = np.ascontiguousarray(score).view("S{}".format(score.shape[1]))
        dnbs = self._createDnbSet(dnb_subset, esr_mask)
        return base, score, dnbs

    ###########################################################################
    #Cal to Fastq IO                                                          #
    ###########################################################################

    def _writeFq(self, fq_path, base, score, strand, dnbs, mode='w'):
        if fq_path.endswith(".gz"):
            writer = gzip.open
            mode += 'b'
        else:
            writer = open
        with writer(fq_path, mode=mode) as fp:
            self._dumpFq(fp, base, score, strand, dnbs)
        return

    def _dumpFq(self, fp, base, score, strand, dnbs):
        strand = "" if strand is None else "/{}".format(strand)
        fqfmtstr = "\n".join(["@{SlideId}L{Lane:d}C{Col:03d}R{Row:03d}_{DNB:06d}{strand}",
                              "{Bases}",
                              "+",
                              "{Scores}\n"])
        fqfmtgen = (util._bundle(DNB=dnb_i + 1, strand=strand,
                            Bases=base[dnb_i, 0], Scores=score[dnb_i,0],
                            **self.header.fileTag) for dnb_i in dnbs)
        self._dumpData(fp, fqfmtstr, fqfmtgen)
        return

    def dumpFq(self, fp=sys.stdout, cycles=None, strand=None, dnb_subset=False, esr_filter=False):
        base, score, dnbs = self.memFq(cycles, strand, dnb_subset, esr_filter)
        self._dumpFq(fp, base, score, strand, dnbs)
        return 

    def writeFqSE(self, fq_path, cycles=None, strand=None, dnb_subset=False, esr_filter=False):
        base, score, dnbs = self.memFq(cycles, strand, dnb_subset, esr_filter)
        self._writeFq(fq_path, base, score, strand, dnbs, mode=self.writeMode)
        return

    def writeFqPE(self, fq_path, cycles=None, dnb_subset=False, esr_filter=False):
        if (self.calCount == 2):
            calPaths = self.calPaths
            #Set up cycles to be the same for each run
            if isinstance(cycles, (np.ndarray, list, tuple)) and (np.array(cycles).ndim == 2):
                pass
            else:
                cycles = [cycles, cycles]
        else:
            calPaths = [self.calPaths] * 2
            
        bases = []
        scores = []
        dnbs = []
        fns = []
        for strand, cal_fn in enumerate(calPaths, 1):
            #Load header for each cal file independently
            self.Open(cal_fn)
            base, score, dnb_set = self.memFq(cycles[strand - 1], strand, dnb_subset, esr_filter)
            bases.append(base)
            scores.append(score)
            dnbs.append(dnb_set)
        dnb_set = dnbs[0].intersection(dnbs[1])

        for strand, cal_fn in enumerate(calPaths, 1):
            idx = strand - 1
            self.Open(cal_fn)
            pe_name = fq_path.replace('.fq', "_{}.fq".format(strand))
            self._writeFq(pe_name, bases[idx], scores[idx], strand, dnb_set, mode=self.writeMode)
        return

    def writeFq(self, fq_path, cycles=None, strand=None, dnb_subset=False, esr_filter=False):
        '''
            Write fastq (compressed format)
            Paremeters:
                fq_path: Save name for fq file
                 cycles: Cycles that will be written to fq file (several different formats)
                        None: Write all cycles in cal file to 
        '''
        #TODO
        #Determine which fq file type should be written to (based on cycles/cal_files)
        pe = (self.calCount == 2)
        if isinstance(cycles, (list, tuple, np.ndarray)):
            cycles = np.array(cycles)
            pe = pe or ((cycles.ndim == 2) and (cycles.shape[1] == 2))
        if pe:
            self.writeFqPE(fq_path, cycles, dnb_subset, esr_filter)
        else:
            self.writeFqSE(fq_path, cycles, strand, dnb_subset, esr_filter)
        return

    def writeFqStat(self, save_path, cycles=None, strand=None, dnb_subset=False, esr_filter=False):
        #TODO
        return

    def writeCal(self):
        #TODO: Low Priority
        return

    def fq_to_cal(self, fq_path):
        #TODO: Low Priority
        return

if __name__ == "__main__":
    if len(sys.argv) == 3:
        cal = CalReaderLite(sys.argv[1])
        cal.writeFq(sys.argv[2])
