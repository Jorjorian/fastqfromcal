'''
Author: Wenlan Tian
Date: 08/11/2016
Purpose: histogram of average intensity
'''

import gzip
from itertools import izip
import multiprocessing as mp
from functools import partial
import glob
import pandas as pd
import os


nucleotides = ['A', 'T', 'G', 'C', 'N']


def reverse_compliment(nucleotide):
    if nucleotide == 'A':
        return 'T'
    elif nucleotide == 'a':
        return 't'
    elif nucleotide == 'T':
        return 'A'
    elif nucleotide == 't':
        return 'a'
    elif nucleotide == 'G':
        return 'C'
    elif nucleotide == 'g':
        return 'c'
    elif nucleotide == 'C':
        return 'G'
    elif nucleotide == 'c':
        return 'g'
    else:
        return nucleotide

def reverse_compliment_seq(seq):
    reverse_compliment_seq = ''

    for nucleotide in seq[::-1]:
        reverse_compliment_seq += reverse_compliment(nucleotide)

    return reverse_compliment_seq

def one_mismatch(seq):
    one_mismatch_seqs = []

    for i in range(len(seq)):
        n = seq[i]

        for c in nucleotides:
            if(n == c):
                continue

            if i==0:
                one_mismatch_seq = c + seq[1:]
            elif i== len(seq)-1:
                one_mismatch_seq = seq[:i] + c
            else:
                one_mismatch_seq = seq[:i] + c + seq[i+1:]

            one_mismatch_seqs.append(one_mismatch_seq)

    return one_mismatch_seqs

def get_one_mismatch_barcodes(barcode):
    one_mismatch_barcodes = []

    one_mismatch_barcodes.append(barcode)

    one_mismatch_barcodes.extend(one_mismatch(barcode))

    #barcode_revCompl = reverse_compliment_seq(barcode)

    #one_mismatch_barcodes.append(barcode_revCompl)

    #one_mismatch_barcodes.extend(one_mismatch(barcode_revCompl))

    return one_mismatch_barcodes


def get_barcode_list_0_mismatch(barcode_list,strand):
    f = open(barcode_list, 'r')

    barcode_ids = []

    barcode_dict = {}

    header = f.readline()

    for line in f:
        barcode_id, barcode = line.rstrip().split('\t')
        barcode_ids.append(barcode_id)

        if strand == 'forward':
        	barcode_dict[barcode] = barcode_id

        elif strand == 'revCompl':
        	barcode_dict[reverse_compliment_seq(barcode)] = barcode_id

        elif strand == 'both':
        	barcode_dict[barcode] = barcode_id
        	barcode_dict[reverse_compliment_seq(barcode)] = barcode_id
        else:
        	print 'wrong strand input!'

    return barcode_dict, barcode_ids

def get_barcode_list_1_mismatch(barcode_list, strand):
    f = open(barcode_list, 'r')

    barcode_ids = []

    barcode_dict = {}

    header = f.readline()

    for line in f:

        barcode_id, barcode = line.rstrip().split('\t')
        one_mismatch_barcodes = get_one_mismatch_barcodes(barcode)

        barcode_ids.append(barcode_id)

        for bc in one_mismatch_barcodes:
        	if strand == 'forward':
        		barcode_dict[bc] = barcode_id

        	elif strand == 'revCompl':
        		barcode_dict[reverse_compliment_seq(bc)] = barcode_id

        	elif strand == 'both':
        		barcode_dict[bc] = barcode_id
        		barcode_dict[reverse_compliment_seq(bc)] = barcode_id

        	else:
        		print 'wrong strand input!'


    return barcode_dict, barcode_ids

def concat_split_rate(slide, lane, mismatch, strand):

    split_rate_files = glob.glob(slide + '_' + lane + '_*_' + mismatch + '_split_rate_' + strand + '.txt')

    print split_rate_files

    df = pd.DataFrame()

    for col in range(1, 7):
        for row in range(1, 73):
            if row < 10:
                fov = 'C00' + str(col) + 'R00' + str(row)
            else:
                fov = 'C00' + str(col) + 'R0' + str(row)

            split_rate_file = slide + '_' + lane + '_'+ fov +'_' + mismatch + '_split_rate_' + strand + '.txt'

            print split_rate_file

            df_current = pd.read_csv(split_rate_file, sep = '\t', header=None)

            df_current.columns= ['barcode_id', fov]

            if df.empty:
                df = df_current
            else:
                df_current = df_current.drop(['barcode_id'], axis=1)

                df = df.merge(df_current, left_index=True, right_index=True)

    df.to_csv(slide + '_' + lane + '_'+ mismatch + '_split_rate_' + strand + '.txt', sep='\t', index=False)

def split_barcode(seq_length, barcode_size, slide, lane, barcode_file, mismatch, strand, fastq1):


    fov = fastq1.split('/')[-1].split('_')[0]

    print fov

    fastq2 = fastq1.replace('_1.fq.gz', '_2.fq.gz')
    fastq_bc = '/'.join(fastq1.split('/')[:-2])[:-1] + 'BC/' + lane + '/' + fov + '.fq.gz'

    print fastq1
    print fastq2
    print fastq_bc


    
    if mismatch == '0_mismatch':
        barcodes, barcode_ids = get_barcode_list_0_mismatch(barcode_file, strand)
    elif mismatch == '1_mismatch':
        barcodes, barcode_ids = get_barcode_list_1_mismatch(barcode_file, strand)

    #print barcodes
    #print barcode_ids

    if not os.path.exists(mismatch):
        os.mkdir(mismatch)

    if not os.path.exists(mismatch + '/' + strand):
        os.mkdir(mismatch + '/' + strand)
    
    files_out1 = {}
    files_out2 = {}

    #for id in barcode_ids:
    #    file1 = gzip.open(mismatch + '/' +strand + '/' + slide+'_'+lane+'_' + fov + '_' + id + '_1.fq.gz', 'w')
    #    file2 = gzip.open(mismatch + '/' +strand + '/' + slide+'_'+lane+'_' + fov + '_' + id + '_2.fq.gz', 'w')
    #    files_out1[id] = file1
    #    files_out2[id] = file2

    line_no = 0

    write = 'na'

    header = ''

    total_reads = 0

    total_reads_demux  = 0

    dict_reads_demux = {}

    

    with gzip.open(fastq1, 'r') as f1, gzip.open(fastq2, 'r') as f2, gzip.open(fastq_bc, 'r') as f_bc:
        for line1, line2, line3 in izip(f1, f2, f_bc):
            if line_no%4==0:
                header = line1.split('/')[0]
                write = 'na'
                total_reads+=1
            elif line_no%4==1:
                barcode_seq = line3.rstrip()[seq_length:]
                #print barcode_seq

                if barcode_seq in barcodes:
                    write = barcodes[barcode_seq]
                    #files_out1[write].write(header + '/1\n')
                    #files_out2[write].write(header + '/2\n')

                    total_reads_demux+=1

                    barcode_id = barcodes[barcode_seq]

                    if barcode_id in dict_reads_demux:
                        dict_reads_demux[barcode_id] += 1
                    else:
                        dict_reads_demux[barcode_id] = 1

            #if write!='na':
            #    files_out1[write].write(line1)
            #    files_out2[write].write(line2)


            line_no+=1

            #print line_no

            #if line_no >= 1000:
            #    break

    #for id in barcode_ids:
    #    files_out1[id].close() 
    #    files_out2[id].close()       

    f_out = open( slide+'_'+lane+'_' + fov + '_' + mismatch + '_split_rate_'+strand+'.txt', 'w')

    #print dict_reads_demux.keys()

    #barcode_ids.sort()

    for barcode_id in barcode_ids:
        if barcode_id in dict_reads_demux:
            f_out.write(str(barcode_id) + '\t' + str(dict_reads_demux[barcode_id]) + '\n')
        else:
            f_out.write(str(barcode_id) + '\t0\n')

    if total_reads > 0:
        f_out.write('\t' + str(total_reads_demux)  + '\n\t' + str(total_reads) + '\n\t' + str(round(float(total_reads_demux)/total_reads * 100, 2)) + '\n')
    else:
        f_out.write('\t' + str(total_reads_demux)  + '\n\t' + str(total_reads) + '\n\t0\n')

def main(slide, lane, barcode_file, strand, seq_length, barcode_size):

    # seq_length = 6
    # barcode_size = 10

    # slide = 'V300031743C'
    # lane = 'L01'

    # barcode_file = '/home/wtian/Projects/Bioinformatics/coverage_database/data/Adam/V300031736B/barcode.txt'

    #strands = ['forward', 'revCompl', 'both']

    mismatch = '1_mismatch'
    #mismatch = '0_mismatch'
    #strand = 'forward'
    strand = 'revCompl'
    #strand = 'both'

    fastq_prefix = '/research/rv-02/home/ajorjorian/fastq_temps/' + slide + '/' + lane + '/'

    print fastq_prefix

    fastqs = glob.glob(fastq_prefix + '*_1.fq.gz')

    #print fastqs

    #split_barcode(seq_length, barcode_size, slide, lane, barcode_file, mismatch, strand, fastqs[0])

    #return

    p = mp.Pool(72)

    split_barcode_partial = partial(split_barcode, seq_length, barcode_size, slide, lane, barcode_file, mismatch, strand)

    p.map(split_barcode_partial, fastqs)

    concat_split_rate(slide, lane, mismatch, strand)


    

if __name__ == "__main__":
    main()
