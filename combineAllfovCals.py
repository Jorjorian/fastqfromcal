import os
import glob
import multiprocessing as mp
from multiprocessing import Pool
from calReader import Cal
import sys
import argparse
usage = 'abc'
import traceback
import json
import subprocess
Barcode_List = '//prod/pv-10/home/ajorjorian/fastq_temps/newBarcode128_10bp.txt'


def append_cycles(cal1, cal2, cal1_stop, cal2_range):
    for i, cycle in enumerate(range(cal2_range[0], cal2_range[1])):
        cal1.basesDigit[cal1_stop + i] = cal2.basesDigit[cycle]
        cal1.bases[cal1_stop + i] = cal2.bases[cycle]
        cal1.qual[cal1_stop + i] = cal2.qual[cycle]
    return cal1


def cal_to_fq(cal_file, fq_temp_dir, lane, cycle_bounds, slide, esr=True, barcode=False, lite=False, q30_only=False,
              esr_type='first'):
    try:
        print(slide)
        if not bool(slide):
            fq_path = os.path.join(fq_temp_dir, os.path.basename(cal_file).replace('.cal', '.fq.gz').replace('.Cal',
                                                                                                             '.fq.gz'))
            if os.path.isfile(fq_path):
                return True
            if type(cycle_bounds[0]) == list:
                p_e = True
            else:
                p_e = False
            if p_e:
                if not os.path.isfile(fq_path.replace('.fq.gz', '_1.fq.gz')):
                    cal_obj = Cal()
                    if lite:
                        cal_obj.loadLite(cal_file)
                    else:
                        cal_obj.load(cal_file)
                    cal_obj.getESRFilter(cycles=cycle_bounds, esr_type=esr_type)
                    # cal_obj.basesDigit.pop(62)
                    # cal_obj.qual.pop(62)
                    if bool(barcode):
                        cal_obj.basesDigit.pop(cycle_bounds[-1][-1]+1)
                        cal_obj.qual.pop(cycle_bounds[-1][-1]+1)
                        for i in range(cycle_bounds[-1][-1] + 1, cycle_bounds[-1][-1] + 1 + barcode):
                            print(i)
                            cal_obj.basesDigit[i] = cal_obj.basesDigit[i + 1]
                            cal_obj.qual[i] = cal_obj.qual[i + 1]
                        cycle_bounds[-1][-1] += barcode
                        cal_obj.qual.pop(cycle_bounds[-1][-1] + 1)
                        cal_obj.basesDigit.pop(cycle_bounds[-1][-1] + 1)
                        # cycle_bounds[-1][-1] -= 1
                    # if barcode:
                    #     bc_json = fq_path.replace('.fq.gz', '_bc.json')
                    #     cal_obj.barcode_processing(Barcode_List, bc_json)
                    cal_obj.writefq(fq_path, os.path.basename(cal_file).replace('.cal', '.fq.gz').replace('.Cal',
                                                                                                          '.fq.gz'),
                                    cycle_bounds,
                                    esr_filter=True)
                fq1 = fq_path.replace('.fq.gz', '_1.fq.gz')
                fq2 = fq_path.replace('.fq.gz', '_2.fq.gz')
                return fq1, fq2
            else:
                if not os.path.isfile(fq_path):
                    cal_obj = Cal()
                    if lite:
                        cal_obj.loadLite(cal_file)
                    else:
                        cal_obj.load(cal_file)
                    if esr_type.upper() == 'WHOLE':
                        cycles = cycle_bounds
                    else:
                        cycles = [cycle_bounds[0]]
                    cal_obj.getESRFilter(cycles=cycles, esr_type=esr_type)
                    # cal_obj.basesDigit.pop(62)
                    # cal_obj.qual.pop(62)
                    if bool(barcode):
                        cal_obj.basesDigit.pop(cycle_bounds[-1]+1)
                        cal_obj.qual.pop(cycle_bounds[-1]+1)
                        for i in range(cycle_bounds[-1]+1, cycle_bounds[-1] + 1 + barcode):
                            print(i)
                            cal_obj.basesDigit[i] = cal_obj.basesDigit[i + 1]
                            cal_obj.qual[i] = cal_obj.qual[i + 1]
                        cycle_bounds[-1] += barcode
                        # cal_obj.qual.pop(cycle_bounds[-1]) + 1
                        # cal_obj.basesDigit.pop(cycle_bounds[-1] + 1)
                        # cycle_bounds[-1] -= 1
                    # if barcode:
                    #     bc_json = fq_path.replace('.fq.gz', '_bc.json')
                    #     cal_obj.barcode_processing(Barcode_List, bc_json)
                    cal_obj.writefq(fq_path, os.path.basename(cal_file).replace('.cal', '').replace('.Cal', ''),
                                    cycle_bounds,
                                    esr_filter=True)
                    # cal_obj.write_fqstat(fq_temp_dir, os.path.basename(cal_file).replace('.cal', '')+'_'+lane,
                    #                      cycle_bounds, False, esr_filter=True)
            return fq_path
        else:
            if not (os.path.isfile(cal_file[0]) and os.path.isfile(cal_file[1])):
                return False
            fq_path = os.path.join(fq_temp_dir, os.path.basename(cal_file[0]).replace('.cal', '.fq.gz').replace('.Cal',
                                                                                                          '.fq.gz'))
            cal_out = cal_file[0].replace(slide[0], slide[-1])
            p_e = True
            if p_e:
                if not os.path.isfile(fq_path.replace('.fq.gz', '_1.fq.gz')):
                    cal_obj = Cal()
                    if not lite:
                        cal_obj.load(cal_file[0], cal_file[1], V40=True)
                        cal_obj.save(cal_out)
                    else:
                        cal_obj.loadLite([cal_file[0], cal_file[1]])
                    if bool(barcode):
                        cal_obj.basesDigit.pop(cycle_bounds[-1][-1] + 1)
                        cal_obj.qual.pop(cycle_bounds[-1][-1] + 1)
                        for i in range(cycle_bounds[-1][-1] + 1, cycle_bounds[-1][-1] + 1 + barcode):
                            print(i)
                            cal_obj.basesDigit[i] = cal_obj.basesDigit[i + 1]
                            cal_obj.qual[i] = cal_obj.qual[i + 1]
                        cycle_bounds[-1][-1] += barcode
                        cal_obj.qual.pop(cycle_bounds[-1][-1] + 1)
                        cal_obj.basesDigit.pop(cycle_bounds[-1][-1] + 1)
                    cal_obj.getESRFilter(cycles=cycle_bounds, esr_vect_path=False, esr_type=esr_type)
                    # if barcode:
                    #     bc_json = fq_path.replace('.fq.gz', '_bc.json')
                    #     cal_obj.barcode_processing(Barcode_List, bc_json)
                    if not q30_only:
                        # pass
                        if not lite:
                            cal_obj.writefq(fq_path, os.path.basename(cal_file[0]).replace('.cal', '') + '_' + lane,
                                            cycle_bounds, esr_filter=True)
                            # cal_obj.write_fqstat(fq_temp_dir,
                            #                      os.path.basename(cal_file[0]).replace('.cal', '').replace('.Cal',
                            #                                                                                '') + '_' + lane,
                            #                      cycle_bounds, False, esr_filter=True)
                        else:
                            cal_obj.writefq(fq_path, os.path.basename(cal_file[0]).replace('.cal', '') + '_' + lane,
                                            cycle_bounds, esr_filter=True)
                            # cal_obj.write_fqstat(fq_temp_dir,
                            #                      os.path.basename(cal_file[0]).replace('.cal',
                            #                                                            '').replace('.Cal',
                            #                                                                        '') + '_' + lane,
                            #                      cycle_bounds, False, esr_filter=True)
                    else:
                        if not lite:
                            cal_obj.write_fqstat(fq_temp_dir,
                                                 os.path.basename(cal_file[0]).replace('.cal', '').replace('.Cal',
                                                                                                           '') + '_' + lane,
                                                 cycle_bounds, False, esr_filter=True,
                                                 q30_only=True)
                        else:
                            cal_obj.write_fqstat(fq_temp_dir,
                                                 os.path.basename(cal_file[0]).replace('.cal', '').replace('.Cal',
                                                                                                           '') + '_' + lane,
                                                 cycle_bounds, False, esr_filter=True,
                                                 q30_only=True)
                    # split_fastq(fq_path, cycles)
                    # os.remove(fq_path)
                fq1 = fq_path.replace('.fq.gz', '_1.fq.gz')
                fq2 = fq_path.replace('.fq.gz', '_2.fq.gz')
                return fq1
            else:
                if not os.path.isfile(fq_path):
                    cal_obj = Cal()
                    cal_obj.load(cal_file)
                    # cal_obj.getESRFilter(cycles=cycle_bounds, esr_vect_path=fq_path.replace('.fq.gz', '.npy'))
                    # cal_obj.basesDigit.pop(62)
                    # cal_obj.qual.pop(62)
                    if bool(barcode):
                        cal_obj.basesDigit.pop(cycle_bounds[-1][-1])
                        cal_obj.qual.pop(cycle_bounds[-1][-1])
                        for i in range(cycle_bounds[-1][-1], cycle_bounds[-1][-1] + 1 + 42):
                            print(i)
                            cal_obj.basesDigit[i] = cal_obj.basesDigit[i + 1]
                            cal_obj.qual[i] = cal_obj.qual[i + 1]
                        cycle_bounds[-1][-1] += 42
                        cal_obj.qual.pop(cycle_bounds[-1][-1] + 1)
                        cal_obj.basesDigit.pop(cycle_bounds[-1][-1] + 1)
                        cycle_bounds[-1][-1] -= 1
                    # if barcode:
                    #     bc_json = fq_path.replace('.fq.gz', '_bc.json')
                    #     cal_obj.barcode_processing(Barcode_List, bc_json)
                    cal_obj.writefq(fq_path, os.path.basename(cal_file).replace('.cal', '').replace('.Cal',''), cycle_bounds,
                                    esr_filter=None)
                    # cal_obj.write_fqstat(fq_temp_dir, os.path.basename(cal_file).replace('.cal', '') + '_' + lane,
                    #                      cycle_bounds, esr_filter=True)
            return fq_path
    except:
        print(traceback.format_exc())
        return False


def proc_cycle_str(cycle_str, offset_str, read_max_str):
    cycle_str = str(cycle_str)
    cycle_list = cycle_str.split('_')
    offset_list = offset_str.split('_')
    read_max_list = read_max_str.split('_')
    cycle_diff = [int(read_max_list[i]) - int(cycle_list[i]) - int(offset_list[i]) for i in range(len(cycle_list))]
    if len(cycle_list) == 2:
        cycle_str = '_'.join(map(str, [0 + int(offset_list[0]), int(cycle_list[0]) + int(offset_list[0]),
                                       int(cycle_list[0]) + int(offset_list[1]) + int(offset_list[0]) + cycle_diff[0],
                                       int(cycle_list[0]) + int(cycle_list[1]) + int(offset_list[1]) +
                                       int(offset_list[0]) + cycle_diff[0]]))
        cycle_list = map(int, cycle_str.split('_'))
        v2_cycle_bounds = [[1+int(cycle_list[0]), int(cycle_list[1])], [int(cycle_list[2])+1, int(cycle_list[3])]]
    else:
        cycle_list = [0 + int(offset_list[0]), int(cycle_list[0]) + int(offset_list[0])]
        cycle_str = '_'.join(map(str, cycle_list))
        v2_cycle_bounds = [1 + int(cycle_list[0]), int(cycle_list[1])]
    return cycle_str, cycle_list, v2_cycle_bounds


def parse_arguments(arguments):
    print(arguments)
    arg_parser = argparse.ArgumentParser(usage=usage)
    # run parameters
    arg_parser.add_argument("-c", "--cal_path", action="store", dest="cal_path",
                            default='\\\\zebra-stor\\zebra-01\\Zebra_Images',
                            help="Path to folder containing .cal Files")
    arg_parser.add_argument("-t", "--temp", action="store", dest="temp_path", default='Fastq_Temp',
                            help="designate temporary data dir")
    arg_parser.add_argument("-H", "--threads", action="store", dest="threads", default='100',
                            help="designate thread count")
    arg_parser.add_argument("-j", "--json_path", action="store", dest="json", default=None,
                            help="designate json directory")

    para, args = arg_parser.parse_known_args()
    return para


def sort_npy(npy):
    fov = os.path.basename(npy).split('.')[0]
    out = (int(fov[1:4]), int(fov[5:]))
    return out


def run_cal_join(arguments):
    para = parse_arguments(arguments)
    json_path = para.json
    threads = int(para.threads)
    with open(json_path, 'r') as rf:
        request_dict = json.load(rf)
    slide = map(str, request_dict['slide'])
    if bool(request_dict['run_tag']):
        run_tag = '-' + str(request_dict['run_tag'])
    else:
        run_tag = ''
    read_lengths = str(request_dict['cycles'])
    ground_truth = str(request_dict['gt_cycles'])
    barcode_length = int(request_dict['barcode'])
    offsets = str(request_dict['offsets'])
    lanes = request_dict['lanes']
    if bool(request_dict['lane_fields']):
        fov_whitelist = request_dict['lane_fields']
    else:
        fov_whitelist = False
    esr = bool(request_dict['esr'])
    esr_type = str(request_dict['esr_type'])
    lite = bool(request_dict['Lite'])
    q30_only = bool(request_dict['Q30_Only'])
    try:
        cycle_str, cycle_list, v2_cycle_bounds = proc_cycle_str(read_lengths, offsets, ground_truth)
        for lane in lanes:
            if bool(fov_whitelist):
                fovs = map(str, fov_whitelist[lane])
            else:
                fovs = False
            cal_pool_list = []
            temp_path = os.path.join(para.temp_path, slide[-1] + run_tag, lane)
            # npy_list = sorted(glob.glob(os.path.join(temp_path, '*.npy')), key=sort_npy)
            # npy_vect = []
            # for f in npy_list:
            #     npy_vect.extend(np.load(f).ravel().tolist())
            # npy_vect = np.ndarray(npy_vect)
            # np.save(os.path.join(os.path.dirname(temp_path), lane + '.npy'), npy_vect)
            if not os.path.isdir(temp_path):
                os.makedirs(temp_path)
            print(slide[-1])
            if len(slide) == 1:
                if lite:
                    cal_files = glob.glob(os.path.join(para.cal_path, slide[-1], lane, 'Cal', '*.Cal'))
                else:
                    cal_files = glob.glob(os.path.join(para.cal_path, slide[-1], lane, 'calFile', '*.cal'))
                if bool(fovs):
                    for cal in cal_files:
                        fov = os.path.basename(cal).split('.')[0]
                        if fov in fovs:
                            cal_files.pop(cal_files.index(cal))
                # print(len(cal_files), cal_files)
                cal_pool = Pool(processes=threads)
                for cal in cal_files:
                    cal_pool_list.append(cal_pool.apply_async(cal_to_fq, args=(cal, temp_path, lane, v2_cycle_bounds,
                                                                               False, esr, barcode_length, lite,
                                                                               q30_only, esr_type)))
                for cal in cal_pool_list:
                    cal.get()
                cal_pool.close()
                cal_pool.join()

                if len(cycle_list) > 2:
                    subprocess.Popen('cat ' + os.path.join(temp_path, '*_1.fq.gz') + ' > ' +
                                     os.path.join(os.path.dirname(temp_path), slide[0] + run_tag + '_' + lane +
                                                  '_1.fq.gz'), shell=True)
                    subprocess.Popen('cat ' + os.path.join(temp_path, '*_2.fq.gz') + ' > ' +
                                     os.path.join(os.path.dirname(temp_path), slide[0] + run_tag + '_' + lane +
                                                  '_2.fq.gz'), shell=True)
                else:
                    subprocess.Popen('cat ' + os.path.join(temp_path, '*.fq.gz') + ' > ' +
                                     os.path.join(os.path.dirname(temp_path), slide[0] + run_tag + '_' + lane +
                                                  '.fq.gz'), shell=True)
            else:
                print('multi_slide')
                if not lite:
                    if not os.path.isdir(os.path.join(para.cal_path, slide[-1], lane, 'calFile')):
                        os.makedirs(os.path.join(para.cal_path, slide[-1], lane, 'calFile'))
                    cal_files_1 = sorted(glob.glob(os.path.join(para.cal_path, slide[0], lane, 'calFile', '*.cal')))
                    cal_files_2 = [cal.replace(slide[0], slide[1]) for cal in cal_files_1]
                else:
                    print(para.cal_path)
                    cal_files_1 = sorted(glob.glob(os.path.join(para.cal_path, slide[0], lane, 'Cal', '*.Cal')))
                    print(cal_files_1)
                    cal_files_2 = [cal.replace(slide[0], slide[1]) for cal in cal_files_1]
                    print(cal_files_2)
                # cal_files_2 = sorted(glob.glob(os.path.join(para.cal_path, slide[1], lane, 'calFile', '*.cal')))[:60]
                cal_files = zip(cal_files_1, cal_files_2)
                cal_pool = mp.Pool(processes=100)
                for cal in cal_files:
                    cal_pool_list.append(cal_pool.apply_async(cal_to_fq, args=(cal, temp_path, lane,
                                                                               v2_cycle_bounds, slide, esr,
                                                                               barcode_length, lite, q30_only,
                                                                               esr_type)))
                cal_pool.close()
                cal_pool.join()
                for cal in cal_pool_list:
                    cal.get()
                if len(cycle_list) > 2:
                    subprocess.Popen('cat ' + os.path.join(temp_path, '*_1.fq.gz') + ' > ' +
                                     os.path.join(os.path.dirname(temp_path), slide[-1] + run_tag + '_' + lane +
                                                  '_1.fq.gz'),
                                     shell=True)
                    subprocess.Popen('cat ' + os.path.join(temp_path, '*_2.fq.gz') + ' > ' +
                                     os.path.join(os.path.dirname(temp_path), slide[-1] + run_tag + '_' + lane +
                                                  '_2.fq.gz'),
                                     shell=True)
                else:
                    subprocess.Popen('cat ' + os.path.join(temp_path, '*.fq.gz') + ' > ' +
                                     os.path.join(os.path.dirname(temp_path), slide[-1] + run_tag + '_' + lane +
                                                  '.fq.gz'),
                                     shell=True)


    except Exception as e:
        print(traceback.format_exc())
        print e
        return
            # if p_e:
            #     f1 = os.path.join(para.temp_path, '*_1.fq.gz')
            #     f2 = os.path.join(para.temp_path, '*_2.fq.gz')
            #     first_out = os.path.join(para.temp_path, para.slide_name + '_' + lane + '_read1.fq.gz')
            #     second_out = os.path.join(para.temp_path, para.slide_name + '_' + lane + '_read2.fq.gz')
            #     os.popen('cat ' + f1 + ' > ' + str(first_out))
            #     os.popen('cat ' + f2 + ' > ' + str(second_out))
            #     time.sleep(900)
            #     os.popen('rm ' + f1)
            #     os.popen('rm ' + f2)
            # else:
            #     fq = os.path.join(para.temp_path, '*.fq.gz')
            #     all_out = os.path.join(para.temp_path, para.slide_name + '_' + lane + '_read.FQ.gz')
            #     os.popen('cat ' + fq + ' > ' + str(all_out))
            #     time.sleep(900)
            #     os.popen('rm ' + fq)
    sys.exit()


if __name__ == '__main__':
    run_cal_join(sys.argv)
