import os
import json
from Tkinter import *
import csv
import tkFileDialog
import glob
import codecs

Server_list = ['lx-basm-08']


def get_most_free():
    serverload = []
    for server in Server_list:
        serverload.append([server, len(glob.glob(os.path.join('\\\\', 'zebra-stor', 'zebra-01',
                                       'Zebra_Temp', 'VSeriesOccupancySubsetting', server, '*.json')))])
        serverload.sort(key=lambda arr: arr[1], reverse=True)
    return serverload[-1][0]


class Window(Frame):

    def __init__(self, master=None):
        self.lane_fields = []
        Frame.__init__(self, master)
        self.master = master
        self.init_window()

    def create_esr_buttons(self):
        if not self.esr_on.get():
            self.esr_menu = OptionMenu(self, self.esr_type, *tuple(['start', 'whole']))
            self.esr_menu.grid(column=3, row=7, sticky=W + S + N + E, padx=4, pady=4)
            self.esr_on.set(True)
        else:
            self.esr_menu.grid_forget()
            self.esr_on.set(False)

    # def populate(self):
    #     if ('V' not in self.slide.get()) and ('S1' not in self.slide.get()):
    #         self.request_list.insert(END, '\nAuto-population is only applicable for V2 slides')
    #         return
    #     data_paths = [os.path.join('\\\\zebra-stor\\zebra-01\\ZebraV2\\Data',
    #                                self.slide.get(), 'Info', 'ExperimentInfo.txt'),
    #                   os.path.join('\\\\zebra-stor\\zebra-01\\ZebraV2.1\\Data',
    #                                self.slide.get(), 'Info', 'ExperimentInfo.txt'),
    #                   os.path.join('\\\\hustor-02\\zebra-02\\ZebraV2\\Data',
    #                                self.slide.get(), 'Info', 'ExperimentInfo.txt'),
    #                   os.path.join('\\\\hustor-02\\zebra-02\\ZebraV2.1\\Data',
    #                                self.slide.get(), 'Info', 'ExperimentInfo.txt'),
    #                   os.path.join('\\\\hustor-02\\zebra-02\\Seq200-2c-4c\\Data',
    #                                self.slide.get(), 'Info', 'ExperimentInfo.txt')
    #                   ]
    #     path_found = False
    #     for i, data_path in enumerate(data_paths):
    #         if not os.path.isfile(data_path):
    #             pass
    #         else:
    #             path_found = True
    #             break
    #     if not path_found:
    #         self.request_list.insert(END, '\nNo ExperimentInfo.txt avaliable for requested slide')
    #         return
    #     v2_fc = v2_flow_dict[i]
    #     self.flowcells_toggle[v2_fc].select()
    #     exp_info_dict = {}
    #     with open(data_path, 'r') as ei:
    #         exp_info = ei.readlines()
    #     for entry in exp_info[1:]:
    #             key = entry.strip('\n').replace('=', ':').split(':')[0]
    #             exp_info_dict[key] = entry.strip('\n').replace('=', ':').split(':')[1]
    #     read1_len = int(exp_info_dict['Read1Len'])
    #     read2_len = int(exp_info_dict['Read2Len'])
    #     print(exp_info_dict)
    #     if read2_len > 0:
    #         if not self.pe.get():
    #             self.pe_button.toggle()
    #     if self.r2o.get() and read2_len > 0:
    #         self.read1_gt.delete(0, 10)
    #         self.read1_gt.insert(10, str(read1_len))
    #         self.read1_len.delete(0, 10)
    #         self.read1_len.insert(10, str(int(read2_len) - 1))
    #         self.read1_off.delete(0, 10)
    #         self.read1_off.insert(10, str(read1_len))
    #     else:
    #         if bool(read1_len):
    #             self.read1_gt.delete(0, 10)
    #             self.read1_gt.insert(10, str(read1_len))
    #             self.read1_len.delete(0, 10)
    #             self.read1_len.insert(10, str(int(read1_len) - 1))
    #             self.read1_off.delete(0, 10)
    #             self.read1_off.insert(10, '0')
    #             self.read2_gt = StringVar()
    #             self.read2_gt.set('')
    #             self.read2_len.delete(0, 10)
    #             self.read2_off.delete(0, 10)
    #             if read2_len > 0:
    #                 self.read2_gt.set(str(read2_len))
    #                 self.read2_len.insert(10, str(int(read2_len) - 1))
    #                 self.read2_off.insert(10, '0')
    #         else:
    #             self.read1_gt.delete(0, 10)
    #             self.read1_gt.insert(10, str(read2_len))
    #             self.read1_len.delete(0, 10)
    #             self.read1_len.insert(10, str(int(read2_len) - 1))
    #             self.read1_off.delete(0, 10)
    #             self.read1_off.insert(10, '0')
    #     try:
    #         sample_list = exp_info_dict['Sample Id'].split('|')
    #     except KeyError:
    #         sample_list = exp_info_dict['DNB Id'].split('|')
    #     for i, sample in enumerate(sample_list):
    #         if 'E' in sample.upper():
    #             ref_vars[i].set('Ecoli')
    #         else:
    #             ref_vars[i].set('Human')
    #     self.request_list.insert(END, '\nSample IDs: L01: %s L02: %s L03: %s L04: %s' % tuple(sample_list))

    def init_window(self):
        self.master.title("Request Slide Fastq Regeneration")
        self.pack(fill=BOTH, expand=1)
        self.slide_lb = Label(self, text='Enter Slide Name')
        self.slide1 = Entry(self, background='black', fg='green', insertbackground='green')
        self.slide2 = Entry(self, background='black', fg='green', insertbackground='green')
        self.slide_out = Entry(self, background='black', fg='green', insertbackground='green')
        send_request = Button(self, text="Send Request", command=self.send_request, bg='grey')
        self.console = Text(self, height=40, width=120, font=("Helvetica", 8),
                            background='black', fg='green', insertbackground='green')
        self.slide_lb.grid(row=0, column=0, sticky=W + S + E + N, padx=4, pady=4)
        self.slide1.grid(row=0, column=1, sticky=W + S + E + N, padx=4, pady=4)
        self.slide2.grid(row=0, column=2, sticky=W + S + E + N, padx=4, pady=4)
        self.slide_out.grid(row=0, column=3, sticky=W + S + E + N, padx=4, pady=4)
        self.l1 = BooleanVar()
        self.l2 = BooleanVar()
        self.l3 = BooleanVar()
        self.l4 = BooleanVar()
        # self.lite= BooleanVar()
        self.q30 = BooleanVar()
        self.run_tag = Entry(self, background='black', fg='green', insertbackground='green')

        self.run_tag.grid(column=3, row=6, sticky=W + S + N + E, padx=4, pady=4)
        self.esr_type = StringVar()
        self.esr_type.set('start')
        self.esr_on = BooleanVar()
        self.select_fov_whitelist = Button(self, text='Select Fov Whitelist', command=self.load_fov_data, bg='grey')
        self.esr_button = Button(self, text="Do ESR Filtering?", bg='grey', command=self.create_esr_buttons)
        self.esr_button.grid(row=7, column=2, sticky=W + S + E + N, padx=4, pady=4)
        self.esr_button.invoke()
        self.r1_len = Entry(self, background='black', fg='green', insertbackground='green')
        self.r2_len = Entry(self, background='black', fg='green', insertbackground='green')
        self.r1_gt = Entry(self, background='black', fg='green', insertbackground='green')
        self.r2_gt = Entry(self, background='black', fg='green', insertbackground='green')
        self.r1_off = Entry(self, background='black', fg='green', insertbackground='green')
        self.r2_off = Entry(self, background='black', fg='green', insertbackground='green')
        self.barcode_len = Entry(self, background='black', fg='green', insertbackground='green')
        self.read1_len_label = Label(self, text='Read1 Window Length')
        self.read2_len_label = Label(self, text='Read2 Window Length')
        self.gt_read1_len_label = Label(self, text='True Read1 Length')
        self.gt_read2_len_label = Label(self, text='True Read2 Length')
        self.read2_len_label = Label(self, text='Read2 Window Length')
        self.read2_off_label = Label(self, text='Read2 Window Offset 2')
        self.run_tag_label = Label(self, text='Label for Request')
        self.run_tag_label.grid(column=2, row=6, sticky=W + S + N + E, padx=4, pady=4)
        self.read1_len_label.grid(column=0, row=2, sticky=W + S + N + E, padx=4, pady=4)
        self.r1_len.grid(column=1, row=2, sticky=W + S + N + E, padx=4, pady=4)
        self.read2_len_label.grid(column=2, row=2, sticky=W + S + N + E, padx=4, pady=4)
        self.r2_len.grid(column=3, row=2, sticky=W + S + N + E, padx=4, pady=4)
        self.read1_off_label = Label(self, text='Read1 Window Offset')
        self.gt_read1_len_label.grid(column=0, row=3, sticky=W + S + N + E, padx=4, pady=4)
        self.r1_gt.grid(column=1, row=3, sticky=W + S + N + E, padx=4, pady=4)
        self.gt_read2_len_label.grid(column=2, row=3, sticky=W + S + N + E, padx=4, pady=4)
        self.r2_gt.grid(column=3, row=3, sticky=W + S + N + E, padx=4, pady=4)
        self.gt_read1_len_label.grid(column=0, row=3, sticky=W + S + N + E, padx=4, pady=4)
        self.gt_read2_len_label.grid(column=2, row=3, sticky=W + S + N + E, padx=4, pady=4)
        self.read1_off_label.grid(column=0, row=4, sticky=W + S + N + E, padx=4, pady=4)
        self.r1_off.grid(column=1, row=4, sticky=W + S + N + E, padx=4, pady=4)
        self.read2_off_label.grid(column=2, row=4, sticky=W + S + N + E, padx=4, pady=4)
        self.r2_off.grid(column=3, row=4, sticky=W + S + N + E, padx=4, pady=4)
        self.barcode_label = Label(self, text='Barcode Len')
        self.barcode_label.grid(column=0, row=6, sticky=W + S + N + E, padx=4, pady=4)
        self.barcode_len.grid(column=1, row=6, sticky=W + S + N + E, padx=4, pady=4)
        self.check1 = Checkbutton(self, text='L01', variable=self.l1, onvalue=True, offvalue=False, indicatoron=False,
                                  bg='grey')
        self.check1.grid(column=0, row=1, sticky=W + S + N + E, padx=4, pady=4)
        self.check1.select()
        self.check2 = Checkbutton(self, text='L02', variable=self.l2, onvalue=True, offvalue=False, indicatoron=False,
                                  bg='grey')
        self.check2.grid(column=1, row=1, sticky=W + S + N + E, padx=4, pady=4)
        self.check2.select()
        self.check3 = Checkbutton(self, text='L03', variable=self.l3, onvalue=True, offvalue=False, indicatoron=False,
                                  bg='grey')
        self.check3.grid(column=2, row=1, sticky=W + S + N + E, padx=4, pady=4)
        self.check3.select()
        self.check4 = Checkbutton(self, text='L04', variable=self.l4, onvalue=True, offvalue=False, indicatoron=False,
                                  bg='grey')
        self.check4.grid(column=3, row=1, sticky=W + S + N + E, padx=4, pady=4)
        self.check4.select()
        self.select_fov_whitelist.grid(column=1, row=7, sticky=W + S + N + E, padx=4, pady=4)
        send_request.grid(row=7, column=0, sticky=W + S + E + N, padx=4, pady=4)
        self.console.grid(sticky=W, columnspan=3)
        # self.lite_mode = Checkbutton(self, text='Lite', variable=self.lite, onvalue=True, offvalue=False,
        #                              indicatoron=False, bg='grey')
        # self.lite_mode.grid(row=7, column=1, sticky=W + S + E + N, padx=4, pady=4)
        self.q30_only = Checkbutton(self, text='Q30_Only', variable=self.q30, onvalue=True, offvalue=False,
                                    indicatoron=False, bg='grey')
        self.q30_only.grid(row=7, column=1, sticky=W + S + E + N, padx=4, pady=4)

    def load_fov_data(self):
        self.lane_fields = []
        self.field_path = str(tkFileDialog.askopenfilename())
        with open(self.field_path, 'r') as f:
            a = csv.reader(codecs.EncodedFile(f, 'utf8', 'utf_8_sig'), delimiter=',')
            for row in a:
                self.lane_fields.append(row[0])
        self.console.insert(END, '%s loaded\n' % self.field_path)


    def send_request(self):
        lanes = []
        for lane in [(self.l1, 'L01'), (self.l2, 'L02'), (self.l3, 'L03'), (self.l4, 'L04')]:
            if bool(lane[0].get()):
                lanes.append(lane[1])
        cycles = self.r1_len.get()
        if bool(self.r2_len.get()):
            cycles += '_'
            cycles += self.r2_len.get()
        gt = self.r1_gt.get()
        if bool(self.r2_gt.get()):
            gt += '_'
            gt += self.r2_gt.get()

        off = self.r1_off.get()
        if bool(self.r2_off.get()):
            off += '_'
            off += self.r2_off.get()
        if bool(self.barcode_len.get()):
            barcode = int(self.barcode_len.get())
        else:
            barcode = 0
        if bool(self.lane_fields):
            lane_field_dict = {'L01': [], 'L02': [], 'L03': [], 'L04': []}
            for entry in self.lane_fields:
                print(entry)
                lane, field = entry.strip().split('_')
                lane_field_dict[lane].append(field)
        else:
            lane_field_dict = False
        if not bool(self.slide2.get()):
            request_dict = {'slide': [self.slide1.get()], 'lanes': lanes, 'gt_cycles': gt, "esr": self.esr_on.get(),
                            "offsets": off, 'cycles': cycles, 'barcode': barcode, 'lane_fields': lane_field_dict,
                            'Q30_Only': self.q30.get(), 'run_tag': self.run_tag.get(),
                            'esr_type': self.esr_type.get()}
        else:
            request_dict = {'slide': [self.slide1.get(), self.slide2.get(), self.slide_out.get()], 'lanes': lanes,
                            'gt_cycles': gt, "esr": self.esr_on.get(), 'esr_type': self.esr_type.get(),
                            "offsets": off, 'cycles': cycles, 'barcode': barcode, 'lane_fields': lane_field_dict,
                            'Q30_Only': self.q30.get(), 'run_tag': self.run_tag.get()}
        if bool(self.run_tag.get()):
            req_name = self.slide1.get() + '-' + self.run_tag.get() + '_' + '_'.join(lanes) + '.json'
        else:
            req_name = self.slide1.get() + '_' + '_'.join(lanes) + '.json'
        server = get_most_free()
        request_path = os.path.join('\\\\', 'zebra-stor', 'zebra-01',
                                    'Zebra_Temp', 'FastqRegenRequests', server, req_name)
        if not os.path.isfile(request_path):
            with open(request_path, 'w') as js:
                json.dump(request_dict, js)
            self.console.insert(END, 'Request Sent\n')
        else:
            self.console.insert(END, 'Request Already Queued\n')




root = Tk()

#root.filename = tkFileDialog.askopenfilename(initialdir = "/",title = "Select file",filetypes = (("jpeg files","*.jpg"),("all files","*.*")))
#size of the window
root.geometry("950x600")

app = Window(root)

root.mainloop()